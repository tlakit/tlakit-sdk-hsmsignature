﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.ServiceModel;
using System.ServiceModel.Channels;
using MessageHeader = System.ServiceModel.Channels.MessageHeader;

namespace TLAKit.HsmSignature.Sdk
{
    public partial class CryptoService
    {
        public string AuthToken { get; }
        public string  ServiceUrl { get; }

        public CryptoService(string authToken)
        {
            this.AuthToken = authToken ?? throw new ArgumentNullException(nameof(authToken));
            this.ServiceUrl = "https://signature.ws.tlakit.com/hsm";
        }
        public CryptoService(string authToken, string serviceUrl) : this(authToken)
        {
            this.ServiceUrl = serviceUrl ?? throw new ArgumentNullException(nameof(serviceUrl));
        }

        public bool VerifySignature(string certificate, string signature, string filepath)
        {
            byte[] hash;
            using (var inputStream = GetBufferedStream(filepath))
            {
                hash = CalculateSha256Hash(inputStream);
            }
            return InternalVerifySignature(certificate, signature, hash);
        }

        public bool VerifySignature(string certificate, string signature, Stream inputStream)
        {
            var hash = CalculateSha256Hash(inputStream);
            return InternalVerifySignature(certificate, signature, hash);
        }

        public SignatureResult Sign(string documentId, string filepath)
        {
            if (!File.Exists(filepath))
            {
                throw new FileNotFoundException("Could not find file.", filepath);
            }

            const int BufferSize = 512 * 1024 * 1024;
            using (var inputStream = new FileStream(filepath, FileMode.Open, FileAccess.Read, FileShare.Read, BufferSize, FileOptions.SequentialScan))
            {
                return Sign(documentId, inputStream);
            }
        }

        public SignatureResult Sign(string documentId, Stream inputStream)
        {
            var hash = CalculateSha256Hash(inputStream);
            return InternalSign(documentId, hash);
        }

        private Stream GetBufferedStream(string filepath)
        {
            if (!File.Exists(filepath))
            {
                throw new FileNotFoundException("Could not find file.", filepath);
            }

            const int BufferSize = 512 * 1024 * 1024;
            return new FileStream(filepath, FileMode.Open, FileAccess.Read, FileShare.Read, BufferSize, FileOptions.SequentialScan);
        }

        private byte[] CalculateSha256Hash(string filepath)
        {
            using (var inputStream = GetBufferedStream(filepath))
            {
                return CalculateSha256Hash(inputStream);
            }
        }

        private byte[] CalculateSha256Hash(Stream inputStream)
        {
            using (SHA256Managed hashstring = new SHA256Managed())
            {
                return hashstring.ComputeHash(inputStream);
            }
        }

        private SignatureResult InternalSign(string documentId, byte[] sha256Hash)
        {
            Binding binding;
            EndpointAddress endpointAddress = new EndpointAddress(this.ServiceUrl);
            if (endpointAddress.Uri.Scheme == "https")
            {
                binding = new BasicHttpBinding(BasicHttpSecurityMode.Transport);
            }
            else
            {
                binding = new BasicHttpBinding(BasicHttpSecurityMode.None);
            }
            var service = new ServiceInterface.CryptoServiceClient(binding, endpointAddress);
            using (var scope = new OperationContextScope(service.InnerChannel))
            {
                var authHeader = MessageHeader.CreateHeader("AuthToken", "http://schemas.tlakit.com/identity", this.AuthToken);
                OperationContext.Current.OutgoingMessageHeaders.Add(authHeader);

                var request = new ServiceInterface.SignatureRequest
                {
                    Base64DocumentHash = Convert.ToBase64String(sha256Hash),
                    DocumentId = documentId
                };
                var response = ((ServiceInterface.ICryptoService)service).Sign(request);

                if (response.Error != null)
                {
                    throw new Exception(ErrorFormatter.Format(response.Error));
                }

                return new SignatureResult()
                {
                    TransactionId = response.TransactionId,
                    SignerCertificate = response.Base64SignerCertificate,
                    Signature = response.Base64Signature
                };
            }
        }

        private bool InternalVerifySignature(string certificate, string signature, byte[] sha256Hash)
        {
            Binding binding;
            EndpointAddress endpointAddress = new EndpointAddress(this.ServiceUrl);
            if (endpointAddress.Uri.Scheme == "https")
            {
                binding = new BasicHttpBinding(BasicHttpSecurityMode.Transport);
            }
            else
            {
                binding = new BasicHttpBinding(BasicHttpSecurityMode.None);
            }
            var service = new ServiceInterface.CryptoServiceClient(binding, endpointAddress);
            using (var scope = new OperationContextScope(service.InnerChannel))
            {
                var authHeader = MessageHeader.CreateHeader("AuthToken", "http://schemas.tlakit.com/identity", this.AuthToken);
                OperationContext.Current.OutgoingMessageHeaders.Add(authHeader);

                var request = new ServiceInterface.VerifySignatureRequest
                {
                    Base64DocumentHash = Convert.ToBase64String(sha256Hash),
                    Base64Signature = signature,
                    Base64SignerCertificate = certificate
                };
                var response = ((ServiceInterface.ICryptoService)service).VerifySignature(request);

                if (response.Error != null)
                {
                    throw new Exception(ErrorFormatter.Format(response.Error));
                }

                return response.IsSignatureValid;
            }
        }
    }
}
