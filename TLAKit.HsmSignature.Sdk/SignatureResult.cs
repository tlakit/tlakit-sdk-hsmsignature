﻿namespace TLAKit.HsmSignature.Sdk
{
    public class SignatureResult
    {
        public string TransactionId { get; set; }
        public string Signature { get; set; }
        public string SignerCertificate { get; set; }
    }
}
