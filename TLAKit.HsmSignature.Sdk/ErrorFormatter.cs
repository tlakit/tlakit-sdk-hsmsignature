﻿using System;
using System.Text;
using TLAKit.HsmSignature.Sdk.ServiceInterface;

namespace TLAKit.HsmSignature.Sdk
{
    public static class ErrorFormatter
    {
        public static string Format(Error error)
        {
            if (error == null) return string.Empty;
            StringBuilder builder = new StringBuilder();
            AppendRecursive(0, builder, error);
            return builder.ToString();
        }

        private static void AppendRecursive(int indentLevel, StringBuilder builder, Error error)
        {
            builder.AppendFormat("{0}Error [Code: {1} '{2}'] Details: {3}", new String('\t' ,indentLevel), error.Code, error.Message, error.Details).AppendLine();
            foreach (var ie in error.InnerErrors)
            {
                AppendRecursive(indentLevel + 1, builder, ie);
            }
        }
    }
}
