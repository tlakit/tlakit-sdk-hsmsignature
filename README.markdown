# Servicio de firma HSM de TLA

## Manual de Integración

Este documento explica cómo solicitar firmas digitales y cómo validar firmas existentes en el servicio de firma con HSM del TLA.

## Funcionalidad

### Operación de Firma 

Este servicio realiza firmas en formato "RSA SHA-256 PKCS#1" que es representada como un arreglo de bytes sin estructura y que no modifica el documento original, es decir, no es una firma 'adjunta'.

Para la operación de firma, el servicio recibe la siguiente información:

* **Documento**: información a firmar, en formato binario, se soporta cualquier tipo de archivo.
* **DocumentId**: que puede ser cualquier de texto que sirva para identificar ese documento, ej. el nombre del archivo, o un ID de algún sistema externo. Este valor no participa en el proceso de firma.

El resultado incluye:

* **Firma**: el valor de la firma digital. Este valor de la firma es independiente del documento y debe ser almacenada junto con el mismo.
* **Certificado** del firmante: el certificado de firma con el que se realizó la misma. Este valor puede ser almacenado como archivo extensión .cer para poder consumirlo con aplicaciones de visualización de certificados. Este certificado será necesario si se desea comprobar la validez de la firma en un futuro.
* **TransactionID**: Un valor único proporcionado por el servicio para identificar la transacción. No participa en el proceso de firma y está unicamente para referirse a una transacción específica en escenarios de diagnóstico de problemas. Se sugiere almacenar junto con la firma en los sistemas que consuman el servicio, pero no es obligatorio.

### Operación de Validación de la firma

La operación de validación de la firma recibe:

* **Documento**: El documento original que fue firmado.
* **Firma**: La firma digital a validar.
* **Certificado**: El certificado con el que se realizó la firma.

Y el resultado es:

* **True/False**: Un valor indicando si la firma es válida (true) o no (false).

## Tecnología

### Plataforma

El servicio de firma de TLA es un WebService SOAP XML que está expuesto en la siguiente ubicación:

https://signature.ws.tlakit.com/hsm

NOTA> Por motivos de seguridad y confidencialidad de la información, este servicio está expuesto únicamente a través de un canal cifrado, por protocolo https.

Además, cuenta con un SDK para Microsoft .NET a través del cual se puede utilizar la funcionalidad sin necesitar integrarse directamente con el Web Service.

### Versiones soportadas:

* SDK: .NET Framework 4.6.2 y superiores

#### Diferencia entre SDK y WebService

La diferencia más importante entre usar el SDK o invocar directamente el WebService, es que el SDK opimiza el proceso de comunicación a través de internet calculando localmente el hash SHA256 del documento original, y únicamente esa información es enviada al servidor, donde se realiza el resto del algoritmo de firmado. De esta manera se evita enviar el documento completo al proceso de firma, recibiendo como ventaja adicional poder realizar firmas de documentos de gran tamaño sin impacto en desempeño.

IMPORTANTE: por lo tanto, si se utiliza directamente el web service, la aplicación cliente debe ser responsable de realizar el proceso de cálculo del hash SHA256 del documento a firmar. Se recomienda por lo tanto usar el SDK en donde sea posible.

## Requisitos para Uso del Servicio

### Autenticación

El único requisito para utilizar el servicio es contar con un token de autenticación provisto por el TLA.

Este token de autenticación debe ser configurado al usar el SDK, o bien, utilizado como encabezado de la petición al WebService.

## Ejemplo de uso de SDK

```csharp

// Crear instancia de la clase cliente incluida en el SDK
// El valor "<token>" se debe reemplazar por el valor proporcionado por el TLA. 

var client = new Sdk.CryptoService("<token>");

// Realizar operación de firma de un archivo existente

var result = client.Sign("documentId", @"C:\ruta\del\archivo.pdf");

// Escribir a consola los resultados de la firma:

Console.WriteLine(String.Format("Service Transaction ID: {0}", result.TransactionId));
Console.WriteLine(String.Format("Resulting Signature: {0}", result.Signature));
Console.WriteLine(String.Format("Signer X509 certificate: {0}", result.SignerCertificate));

// Verificar la firma:
var valid = client.VerifySignature(result.SignerCertificate, result.Signature, filepath);

// Escribir a consola si la firma es válda o no:
Console.WriteLine(String.Format("Server response: Signature is: {0}", valid));
```
