﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TLAKit.HsmSignature.Client
{
    class Program
    {
        static void Main(string[] args)
        {
            if(args.Length < 1)
            {
                Console.WriteLine("Must provide file path as first argument.");
                return;
            }

            var filepath = args[0];

            string authToken = ConfigurationManager.AppSettings["TLAKit.AuthToken"];

            Console.WriteLine("\nGenerating signature for file: " + filepath);

            var client = new Sdk.CryptoService(authToken);

            string documentId = Guid.NewGuid().ToString();
            Console.WriteLine("\nDemo documet Id: " + documentId);

            try
            {
                var result = client.Sign(documentId, filepath);

                Console.WriteLine("\n\nService response: ");
                Console.WriteLine(String.Format("\tService Transaction ID: {0}\n", result.TransactionId));
                Console.WriteLine(String.Format("\tSignature: {0}\n", result.Signature));
                Console.WriteLine(String.Format("\tSigner X509 certificate: {0}\n", result.SignerCertificate));

                Console.WriteLine("\nVerifying the signature...");

                var valid = client.VerifySignature(result.SignerCertificate, result.Signature, filepath);
                Console.WriteLine(String.Format("\tServer response: Signature is valid: {0}", valid));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}
